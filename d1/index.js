// console.log("Hello World");

/*
	Miniactivity
		create a fxn
			create a let variable nickname, the value should come from a prompt();
			log in the console the var

		invoke the fxn
*/
/*
function callNick(){
	let nickName = prompt("Enter nickname:");
	console.log(nickName);
}

callNick();
*/

// Consider this function
function printName(name){
	console.log("Hello " + name);
}


// we can directly pass data into the function, the data is referred to as the "name" within the printName function.
// "name" is calles the PARAMETER
		// PARAMETERS act as named variable/containers that exists only inside a function.


// "Joana", the information provided directly into the function, is call an ARGUMENT. Values passed when invoking a fxn are called arguments. These arguments are then stored as the parameters within the fxn.
printName("Joana");
printName("John");
printName("Jane");


let sampleVar = "Yua";
// you can also utilize variables as arguments when calling functions with parameters.
printName(sampleVar);


/*
	Miniactivity
		create 2 functions
			1st function should have 2 parameters
				log in the console the message "the numbers passed as arguments are: "
				log in the console the 1st parameter (number)
				log in the console the 2st parameter (number)

			invoke the fxn

			2nd fxn should have 3 parameters 
				log in console "My friends are p1,p2,p3"

			invoke the fxn	
*/

function printNumbers(num1,num2){
	console.log("The numbers passed as arguments are:");
	console.log(num1);
	console.log(num2);
}

printNumbers(1,2);

function printFriends(friend1,friend2,friend3){
	console.log("My friends are " + friend1 + ", " + friend2 + ", and " + friend3 + ".")
}

printFriends("Anya","Loid", "Yor");

// improving our last activity with the use of parameters.
function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " is " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
// We can also do the same using prompt(). However, take note that using prompt() outputs a string. 
// Strings are not ideal and cannot be used for mathematical computations.

/*
	Miniactivity
		improve the activity for the isDivisibleBy4 by using parameters
*/


function checkDivisibilityBy4(num){
		let remainder = num % 4;
		console.log("The remainder of " + num + " is " + remainder);
		let isDivisibleBy4 = remainder === 0;
		console.log("Is " + num + " divisible by 4?");
		console.log(isDivisibleBy4);
	}	

checkDivisibilityBy4(348);
checkDivisibilityBy4(501);

console.log("---")

function isEven(num){
	console.log(num % 2 === 0); 
}	

function isOdd(num1){
	console.log(num1 % 2 !== 0);
}

isEven(20);
isEven(21);
isOdd(31);
isOdd(30);

/*
	FUNCTION AS ARGUMENTS
		fxn parameters can also accept other functions as arguments
		some complex functions use other functions to perform complicated tasks/results
*/
function argumentFunction() {
	console.log("This function is passed into another function.")
}

function invokeFunction(functionParameter){
	console.log("This is from invokeFunction.")
	functionParameter();
}
// adding and removing of () impacts the output of JS heavily. When a function is used with (), it denotes that invoking a fxn - that is why when we call a fxn, the function parameter as an argument does not have any (); 

// if the fxn as parameter does not have (), the function as an ARGUMENT should have (), or vice-versa (EITHER OR should have parenthesis)

invokeFunction(argumentFunction);


function printFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

printFullName("Juan",'Dela',"Cruz");
 

// trying to acheive the same result as the function above.
 let firstName = "Juan";
 let middleName = "Dela";
 let lastName = "Cruz";

 console.log(firstName + " " + middleName + " " + lastName);



// Return Statement
	// allows us to output a value from a fxn to pass to the line/block of code that invoked/called our fxn
function returnFullName(firstName, middleName, lastName){
	return firstName + " " + middleName + " " + lastName;
	console.log("This message will not be printed.")
		// will not be printed, any line after a RETURN statement is ignored.
}
/*
this creates a value based on the return statement in the returnFullName fxn.
returnFullName("Jeffrey","Smith","Bezos");	
*/

console.log(returnFullName("Jeffrey","Smith","Bezos"));

// alert(returnFullName("Jeffrey","Smith","Bezos"));
// alert() can also be used to call return values.

let completeName = returnFullName("Jeffrey","Smith","Bezos");
console.log(completeName);
// you can also assign return values to a variable.

/*
	Mini-activity
		make use of 2 parameters to show the full address of a person
			through the use of return statement, create a value for the fxn once it is called passing 2 arguments inside the function.

		log in the console the returned value for the fxn.
*/


function returnAddress(city,state){
	return city + ", " + state;
}

let myAddress = returnAddress("Las Vegas", "Nevada");
console.log(myAddress);

/*
function printPlayerInfo(username, level, job){
	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + job);
}
*/
function printPlayerInfo(username, level, job){
	return "Username: " + username  + "\nLevel: " + level + "\nJob: " + job;
}

let user1 = printPlayerInfo("Yor", 99, "Assassin Cross");

console.log(user1);


